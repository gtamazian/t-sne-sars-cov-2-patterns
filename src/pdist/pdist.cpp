#include <fastahack/Fasta.h>
#include "blosum.hpp"

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

struct Proteins {
  std::vector<std::string> names;
  std::vector<std::string> sequences;
};

Proteins read_proteins(const std::string &path) {
  FastaReference fasta_file;
  fasta_file.open(path);
  Proteins result;
  for (auto &k : fasta_file.index->sequenceNames) {
    result.names.push_back(k);
    result.sequences.push_back(fasta_file.getSequence(k));
  }
  return result;
}

bool is_gap(char base) {
  return base == 'X' or base == '-';
}

float blosum_distance(const std::string &first, const std::string &second, const Blosum::Scores &scores) {
  int numerator = 0;
  int denominator = 0;
  for (size_t k = 0; k < first.length(); k++) {
    if (!is_gap(first[k]) && !is_gap(second[k])) {
      if (first[k] != second[k])
	numerator += scores.get(first[k], second[k]);
      denominator++;
    }
  }
  return denominator > 0 ? static_cast<float>(std::abs(numerator)) / denominator : 0;
}

void print_pairwise_distances(const std::vector<std::string> &sequences, const Blosum::Scores &scores)
{
  auto length = sequences.size();
  for (size_t j = 0; j < length; j++) {
    for (size_t k = 0; k < j; k++)
      printf("0 ");
    for (size_t k = j; k < length; k++)
      printf("%f ", blosum_distance(sequences[j], sequences[k], scores));
    puts("");
  }
}

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cerr << "Usage: get_pairwise_distances proteins.fa blosum62.txt" << std::endl;
    std::exit(EXIT_FAILURE);
  }
  Proteins proteins = read_proteins(argv[1]);
  Blosum::Scores scores{argv[2]};
  print_pairwise_distances(proteins.sequences, scores);
}
