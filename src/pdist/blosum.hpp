#ifndef BLOSUM_HPP
#define BLOSUM_HPP

#include <map>
#include <string>
#include <utility>
#include <vector>

namespace Blosum {
class MissingScores;
class IncorrectTableSize;

class Scores {
 public:
  Scores(const std::string &path);
  int get(char x, char y) const;
  std::vector<char> get_keys() const;

 private:
  std::map<std::pair<char, char>, int> scores_;
};
}  // namespace Blosum

#endif // BLOSUM_HPP
