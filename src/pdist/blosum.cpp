#include "blosum.hpp"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <istream>
#include <iterator>
#include <map>
#include <ostream>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

namespace Blosum {

std::vector<char> parse_header(const std::string &line) {
  std::vector<char> result;
  std::istringstream ss{line};
  std::copy(std::istream_iterator<char>{ss}, std::istream_iterator<char>{},
            std::back_inserter(result));
  return result;
}

struct ScoreLine {
  char header;
  std::vector<int> scores;
};

std::istream &operator>>(std::istream &is, ScoreLine &result) {
  std::string input_line;
  std::getline(is, input_line);
  std::istringstream ss{input_line};
  ss >> result.header;
  std::vector<int> scores;
  std::copy(std::istream_iterator<int>{ss}, std::istream_iterator<int>{},
            std::back_inserter(scores));
  result.scores = scores;
  return is;
}

bool check_score_line_sizes(const std::vector<ScoreLine> &score_lines,
                            size_t required_size) {
  return std::all_of(score_lines.cbegin(), score_lines.cend(),
                     [required_size](const ScoreLine &x) {
                       return x.scores.size() == required_size;
                     });
}

class MissingScore {};

class IncorrectTableSize {};

int Scores::get(char x, char y) const {
  if (x > y) std::swap(x, y);
  if (auto search_result{scores_.find(std::pair{x, y})};
      search_result != scores_.cend())
    return search_result->second;
  throw MissingScore{};
}

std::vector<char> Scores::get_keys() const {
  std::vector<char> keys;
  for (const auto &k : scores_) {
    keys.push_back(k.first.first);
  }
  return keys;
}

Scores::Scores(const std::string &path) : scores_{} {
  std::ifstream ifs{path};
  std::string line;

  do {
    std::getline(ifs, line);
  } while (*std::begin(line) == '#');
  auto header = parse_header(line);

  size_t num_columns = header.size();
  std::vector<ScoreLine> score_lines;
  std::copy_n(std::istream_iterator<ScoreLine>{ifs}, num_columns,
              std::back_inserter(score_lines));

  if (!check_score_line_sizes(score_lines, num_columns))
    throw IncorrectTableSize{};

  for (const auto &[j, j_scores] : score_lines)
    for (size_t idx = 0; idx < num_columns; idx++)
      if (char k = header[idx]; j <= k)
        scores_.insert(std::pair{std::pair{j, k}, j_scores[idx]});
}

}  // namespace Blosum
