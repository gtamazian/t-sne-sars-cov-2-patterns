# t-SNE SARS-CoV-2 Patterns

This repository contains a pipeline that implements computational
analysis presented in the following paper:

Gaik Tamazian, Andrey B. Komissarov, Dmitry Kobak, Dmitry Polyakov,
Evgeny Andronov, Sergei Nechaev, Sergey Kryzhevich, Yuri Porozov, and
Eugene Stepanov. (2022). t-SNE Highlights Phylogenetic and Temporal
Patterns of SARS-CoV-2 Spike and Nucleocapsid Protein Evolution. In:
Bansal, M.S., Cai, Z., Mangul, S. (eds) Bioinformatics Research and
Applications. ISBRA 2022. Lecture Notes in Computer Science,
vol 13760. Springer,
Cham. https://doi.org/10.1007/978-3-031-23198-8_23

The pipeline is implemented in
[Snakemake](https://snakemake.github.io/).
