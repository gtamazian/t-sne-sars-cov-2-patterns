#!/usr/bin/env python3

import gzip
import sys

from pygenomics import fasta


def load_sequences(fasta_path: str) -> list[fasta.Record]:
    """Load all sequences from the specified FASTA file."""
    with open(fasta_path, encoding="utf-8") as fasta_file:
        return list(fasta.read(fasta_file))


def p_distance(first: str, second: str) -> float:
    """Calculte the p-distance between two amino acid sequences."""
    assert len(first) == len(second)

    def not_gap(character: str) -> bool:
        """Check that the character is not a gap in an amino acid sequence."""
        return character not in {"-", "X"}

    nominator = 0
    denominator = 0
    for j, k in zip(first, second):
        if not_gap(j) and not_gap(k):
            nominator += j != k
            denominator += 1

    return nominator / denominator


def produce_pairwise_distances(out_path: str, sequences: list[fasta.Record]) -> None:
    """Write the matrix of pairwise distances to the specified file."""
    with gzip.open(out_path, "wt", encoding="utf-8") as out_file:
        num_sequences = len(sequences)
        for j in range(num_sequences):
            print(
                str.join(
                    "\t",
                    (
                        str.format("{:g}", p_distance(sequences[j].seq, k.seq))
                        for k in sequences
                    ),
                ),
                file=out_file,
            )


def print_sequence_names(out_path: str, sequences: list[fasta.Record]) -> None:
    """Write sequence names in the same order as in the distance matrix."""
    with open(out_path, "w", encoding="utf-8") as out_file:
        for k in sequences:
            print(k.name, file=out_file)


def main(aln_path: str, out_mat_path: str, out_name_path: str) -> None:
    """The main function of the script."""
    sequences = load_sequences(aln_path)
    produce_pairwise_distances(out_mat_path, sequences)
    print_sequence_names(out_name_path, sequences)


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print(
            "Usage: print compute_pairwise_distances.py alignment.fa "
            "distances.txt.gz sequence_names.txt",
            file=sys.stderr,
        )
        sys.exit(1)
    main(sys.argv[1], sys.argv[2], sys.argv[3])
