#!/usr/bin/env python3

"""Get candidates for simplex vertices."""

import argparse
import csv
import itertools
import math
import operator
from typing import Iterator, Optional

import numpy as np
import numpy.linalg as la
from numpy.typing import NDArray
from tqdm import tqdm


Groups = list[tuple[str, list[str]]]


def get_pangolin_groups(path: str) -> Groups:
    """Get protein accessions grouped by their pangolin IDs."""
    with open(path, encoding="utf-8") as csv_file:
        reader = csv.reader(csv_file)
        _ = next(reader)  # skip the header
        return [
            (pangolin, [j[0] for j in accessions])
            for pangolin, accessions in itertools.groupby(
                sorted(((k[0], k[2]) for k in reader), key=operator.itemgetter(1)),
                key=operator.itemgetter(1),
            )
        ]


Indices = list[tuple[str, int]]


def get_protein_indices(path: str) -> Indices:
    """Get indices of protein sequences."""
    with open(path, encoding="utf-8") as index_file:
        return [(str.rstrip(k), j) for j, k in enumerate(index_file)]


def filter_groups(groups: Groups, pangolins: set[str]) -> Groups:
    """Filter groups for the specified pangolin IDs."""
    return [k for k in groups if k[0] in pangolins]


def iterate_candidates(groups: Groups, indices: Indices) -> Iterator[list[int]]:
    """Iterate indices of candidate simplex vertex sets."""
    indices_dict = dict(indices)
    return (
        list(j)
        for j in itertools.product(
            *[[indices_dict[accession] for accession in k] for _, k in groups]
        )
    )


Distances = dict[tuple[int, int], float]


def get_distance_matrix(
    distances: NDArray[np.float64], candidates: list[int]
) -> NDArray[np.float64]:
    """Iterate distance between the candidates."""
    size = len(candidates)
    dist_matrix = np.zeros((size, size))
    for j in range(size):
        for k in range(j, size):
            dist_matrix[j, k] = distances[candidates[j], candidates[k]]
    return dist_matrix + np.transpose(dist_matrix)


MAT_C = np.eye(4) - np.ones(4) / 4


def check_distance_matrix(dist_matrix: NDArray[np.float64]) -> bool:
    """Check if a distance matrix is correct for a simplex."""
    try:
        _ = la.cholesky(-0.5 * MAT_C @ np.square(dist_matrix) @ MAT_C)
        return True
    except la.LinAlgError:
        return False


def check_triangle_inequality(distances: tuple[float, float, float]) -> bool:
    """Check that the pairwise distances satisfy the triangle inequality."""
    return (
        distances[0] <= distances[1] + distances[2]
        and distances[1] <= distances[0] + distances[2]
        and distances[2] <= distances[0] + distances[1]
    )


DIST_INDICES = [
    list(itertools.combinations(k, 2)) for k in itertools.combinations(range(4), 3)
]


PAIR_INDICES = list(itertools.combinations(range(0, 4), 2))


def check_matrix_te(dist_matrix: NDArray[np.float64]) -> bool:
    """Check if the triangle inequality holds for distances from a matrix."""

    def extract_distances(indices: list[tuple[int, int]]) -> tuple[float, float, float]:
        """Extract distances from the matrix."""
        assert len(indices) == 3
        return (
            dist_matrix[indices[0][0], indices[0][1]],
            dist_matrix[indices[1][0], indices[1][1]],
            dist_matrix[indices[2][0], indices[2][1]],
        )

    return all(check_triangle_inequality(extract_distances(k)) for k in DIST_INDICES)


def parse_args(args: Optional[list[str]] = None) -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(description="Get candidates for simplex vertices.")
    parser.add_argument("seq_summary", help="CSV file of protein sequence summaries")
    parser.add_argument("seq_accessions", help="TXT file of protein accessions")
    parser.add_argument("distances", help="TXT.GZ file of pairwise distances")
    parser.add_argument("-f", "--full", help="print distances between the candidates")
    parser.add_argument(
        "-q", "--quiet", action="store_true", help="disable progress bar"
    )
    parser.add_argument(
        "-m",
        "--max-group-size",
        type=int,
        help="maximum number of sequences in a group",
    )
    parser.add_argument("pangolins", nargs="+")
    return parser.parse_args(args)


def get_simplex_candidates(args: argparse.Namespace) -> None:
    """Main function of the script."""
    distances = np.loadtxt(args.distances)
    pangolins = set(args.pangolins)
    protein_indices = get_protein_indices(args.seq_accessions)

    Candidate = tuple[list[int], NDArray[np.float64]]

    def reduce_group(
        group: tuple[str, list[str]], max_size: int
    ) -> tuple[str, list[str]]:
        """Reduce the group if its number of elements is greater than the size."""
        label, elements = group
        return (label, elements[:max_size] if len(elements) > max_size else elements)

    def select_sequences() -> tuple[int, int, list[Candidate]]:
        """Select candidate sequences."""
        results: list[Candidate] = []
        filtered_groups = filter_groups(
            get_pangolin_groups(args.seq_summary), pangolins
        )
        if args.max_group_size is not None:
            filtered_groups = [
                reduce_group(k, args.max_group_size) for k in filtered_groups
            ]

        embedding_failures = 0
        triangle_failures = 0

        for k in tqdm(
            iterate_candidates(
                filtered_groups,
                protein_indices,
            ),
            total=math.prod(len(j) for _, j in filtered_groups),
            disable=args.quiet,
        ):
            dist_matrix = get_distance_matrix(distances, k)
            if not check_matrix_te(dist_matrix):
                embedding_failures += 1
                continue
            if not check_distance_matrix(dist_matrix):
                triangle_failures += 1
                continue
            list.append(results, (k, dist_matrix))
        return (triangle_failures, embedding_failures, results)

    def write_full_file(path: str, records: list[Candidate]) -> None:
        """Produce the full file that contains the distances."""
        index_to_accn_dict = {k: j for j, k in protein_indices}
        with open(args.full, "w", encoding="utf-8") as full_file:
            writer = csv.writer(full_file)
            for group_index, (groups, dist_matrix) in enumerate(records, start=1):
                for j, k in PAIR_INDICES:
                    writer.writerow(
                        [
                            str.format("GROUP_{:d}", group_index),
                            index_to_accn_dict[groups[j]],
                            index_to_accn_dict[groups[k]],
                            dist_matrix[j, k],
                        ]
                    )

    def print_candidates(
        triangle_failures: int, embedding_failures: int, records: list[Candidate]
    ) -> None:
        """Print accessions of selected candidates."""
        index_to_accn_dict = {k: j for j, k in protein_indices}
        print(str.format("# Embedding failures: {:d}", embedding_failures))
        print(str.format("# Triangle inequality failures: {:d}", triangle_failures))
        for k, (group, _) in enumerate(records, start=1):
            print(
                str.format(
                    "GROUP_{:d}\t{:s}",
                    k,
                    str.join("\t", (index_to_accn_dict[j] for j in group)),
                )
            )

    tri_failures, emb_failures, candidates = select_sequences()

    if args.full is not None:
        write_full_file(args.full, candidates)

    print_candidates(tri_failures, emb_failures, candidates)


if __name__ == "__main__":
    get_simplex_candidates(parse_args())
