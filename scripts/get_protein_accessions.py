#!/usr/bin/env python3

"""Get protein accessions for selected records."""

import csv
import itertools
import json
import sys
from enum import Enum, auto


def get_accessions(path: str) -> list[str]:
    """Get accessions of sequences for selected records."""
    with open(path, encoding="utf-8") as in_file:
        reader = csv.reader(in_file)
        return [k[0] for k in itertools.islice(reader, 1, None)]


class ErrorType(Enum):
    """Types of errors that may occur while searching for protein genes."""

    MISSING_GENE = auto()
    MULTIPLE_CDS = auto()


class Error(Exception):
    """Indicates an error related to identifying gene proteins."""

    error_type: ErrorType
    gene: str
    line: str

    def __init__(self, error_type: ErrorType, gene: str, line: str):
        super().__init__(error_type, gene, line)
        self.error_type = error_type
        self.gene = gene
        self.line = line


def get_protein_ids(
    json_path: str, accessions: set[str], gene: str
) -> list[tuple[str, str]]:
    """Get sequence-protein accession pairs for the specified gene."""
    result: list[tuple[str, str]] = []
    with open(json_path, encoding="utf-8") as json_file:
        for line in json_file:
            record = json.loads(line)
            if record["accession"] not in accessions:
                continue
            record_genes = {
                k["name"]: k["cds"] for k in record["annotation"]["genes"] if "cds" in k
            }
            if gene not in record_genes:
                continue
                # raise Error(ErrorType.MISSING_GENE, gene, line)
            if len(record_genes[gene]) > 1:
                raise Error(ErrorType.MULTIPLE_CDS, gene, line)
            list.append(
                result,
                (
                    record["accession"],
                    record_genes[gene][0]["protein"]["accessionVersion"],
                ),
            )

    return result


def extract_proteins(json_path: str, csv_path: str, gene_name: str) -> None:
    """The main function of the script."""
    writer = csv.writer(sys.stdout)
    writer.writerows(
        (
            [seq_id, prot_id]
            for seq_id, prot_id in get_protein_ids(
                json_path, set(get_accessions(csv_path)), gene_name
            )
        )
    )


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print(
            "Usage: python3 get_protein_accessions.py data_report.jsonl sequences.csv "
            "gene_name",
            file=sys.stderr,
        )
        sys.exit(1)
    extract_proteins(sys.argv[1], sys.argv[2], sys.argv[3])
