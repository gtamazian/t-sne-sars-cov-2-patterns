"""Extract proteins which accessions are specified in the table."""

import csv
import itertools
import sys
from collections.abc import Iterator

from pygenomics import fasta


def read_protein_accessions(csv_path: str) -> list[str]:
    """Read protein accessions from the table."""
    with open(csv_path, encoding="utf-8") as csv_file:
        reader = csv.reader(csv_file)
        return [k[1] for k in reader]


class MultipleRecords(Exception):
    """Indicates that multiple records are present for one protein."""

    accession: str

    def __init__(self, accession: str):
        super().__init__(accession)
        self.accession = accession


def read_protein_sequences(
    fasta_path: str, protein_accessions: set[str]
) -> Iterator[fasta.Record]:
    """Iterate sequences of the specified proteins."""
    with open(fasta_path, encoding="utf-8") as fasta_file:
        for accession, records in itertools.groupby(
            fasta.read(fasta_file), lambda x: str.split(x.name, ":", 1)[0]
        ):
            if accession in protein_accessions:
                record_list = list(records)
                if len(record_list) > 1:
                    raise MultipleRecords(accession)
                yield fasta.Record(
                    accession, record_list[0].comment, record_list[0].seq
                )


def extract_proteins(fasta_path: str, csv_path: str) -> None:
    """Extract sequences of proteins according to the table."""
    for record in read_protein_sequences(
        fasta_path, set(read_protein_accessions(csv_path))
    ):
        fasta.write(sys.stdout, record)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(
            "Usage: python3 extract_proteins.py fasta_file gene_proteins.csv",
            file=sys.stderr,
        )
        sys.exit(1)
    extract_proteins(sys.argv[1], sys.argv[2])
