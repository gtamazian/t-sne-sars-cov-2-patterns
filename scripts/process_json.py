#!/usr/bin/env python3

import csv
import json
import sys
from dataclasses import dataclass
from typing import Optional


@dataclass
class Record:
    """NCBI SARS2 virus data record."""

    accession: str
    pangolin: str
    isolate: str
    location: str
    collection_date: str

    def to_list(self) -> list[str]:
        """Get the list of record entries."""
        return [
            self.accession,
            self.pangolin,
            self.isolate,
            self.location,
            self.collection_date,
        ]

    @staticmethod
    def of_string(line: str) -> Optional["Record"]:
        """Parse the record from a line.

        The line must contain all required fields and correspond to
          the record with a complete nucleotide sequence, otherwise
          `None` is returned.

        """
        record = json.loads(line)
        if record["nucleotideCompleteness"] != "complete":
            return None
        try:
            return Record(
                record["accession"],
                record["virus"]["pangolinClassification"],
                record["isolate"]["name"],
                record["location"]["geographicLocation"],
                record["isolate"]["collectionDate"],
            )
        except KeyError:
            return None


def process_json(path: str) -> None:
    """Process records from the NCBI SARS2 data set JSON file."""
    writer = csv.writer(sys.stdout)

    with open(path, encoding="utf-8") as json_file:
        writer.writerows(
            map(
                Record.to_list,
                filter(None, (Record.of_string(line) for line in json_file)),
            )
        )


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage process_json.py data_report.jsonl", file=sys.stderr)
        sys.exit(1)
    process_json(sys.argv[1])
