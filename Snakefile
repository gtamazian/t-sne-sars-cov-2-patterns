rule all:
    input:
        expand("output/distance_plots/{gene}.png", gene=["N", "S"]),
        expand(
            "output/tsne/gene_{gene}_p_{perplexity}.csv",
            gene=[
                "N",
                "S",
                "M",
                "E",
                "ORF3a",
                "ORF6",
                "ORF7a",
                "ORF7b",
                "ORF8",
                "ORF10",
            ],
            perplexity=[5, 10, 30, 50],
        ),
        expand(
            "output/tsne/gene_{gene}_p_100.csv",
            gene=[
                "N",
                "S",
                "M",
                "E",
                "ORF3a",
                "ORF6",
                "ORF7a",
                "ORF7b",
                "ORF8",
            ],
        ),


rule get_sample_table:
    input:
        "input/ncbi_dataset/data/data_report.jsonl",
    output:
        "data/records.csv",
    shell:
        "python3 scripts/process_json.py {input} > {output}"


rule select_records:
    input:
        "data/records.csv",
    output:
        plot="data/sample_index.png",
        table="data/selected_records.csv",
    script:
        "scripts/select_samples.R"


rule get_protein_accessions:
    input:
        json="input/ncbi_dataset/data/data_report.jsonl",
        csv="data/selected_records.csv",
    output:
        "data/protein_accessions/{gene}.csv",
    params:
        gene_name=lambda wildcards: wildcards.gene,
    shell:
        "python3 scripts/get_protein_accessions.py {input.json} {input.csv} "
        "{params.gene_name} > {output}"


rule extract_proteins:
    input:
        fasta="input/ncbi_dataset/data/protein.faa",
        csv="data/protein_accessions/{gene}.csv",
    output:
        "data/protein_sequences/{gene}.fa",
    shell:
        "python3 scripts/extract_proteins.py {input.fasta} {input.csv} > {output}"


rule add_reference_proteins:
    input:
        ref="input/reference/ref-{gene}.fasta",
        rest="data/protein_sequences/{gene}.fa",
    output:
        "data/protein_sequences_with_ref/{gene}.fa",
    shell:
        "cat {input} > {output}"


rule deduplicate_proteins:
    input:
        "data/protein_sequences_with_ref/{gene}.fa",
    output:
        "data/protein_sequences_with_ref_dedup/{gene}.fa",
    shell:
        "python3 scripts/pygenomics_examples/deduplicate_fasta.py "
        "{input} {output}"


rule align_proteins:
    input:
        "data/protein_sequences_with_ref_dedup/{gene}.fa",
    output:
        "data/protein_alignments/{gene}_alignment.fa",
    threads: workflow.cores
    shell:
        "mafft --thread {threads} {input} > {output}"


rule compute_distances:
    input:
        fasta="data/protein_alignments/{gene}_alignment.fa",
        blosum="input/blosum62.txt",
    output:
        "data/distances/{gene}.txt.gz",
    shell:
        "bin/pdist {input.fasta} {input.blosum} | gzip --stdout > {output}"


rule get_sequence_names:
    input:
        "data/protein_alignments/{gene}_alignment.fa",
    output:
        "data/distances/{gene}_seq_names.txt",
    shell:
        "fgrep '>' {input} | cut -f1 -d' ' | tr -d '>' > {output}"


rule generate_sequence_summary:
    input:
        seq_names="data/distances/{gene}_seq_names.txt",
        proteins="data/protein_accessions/{gene}.csv",
        selected="data/selected_records.csv",
        records="data/records.csv",
    output:
        "data/summaries/{gene}.csv",
    script:
        "scripts/generate_summary.R"


rule report_duplicates:
    input:
        "data/protein_sequences_with_ref/{gene}.fa",
    output:
        temp("data/protein_sequences_with_ref_dedup/{gene}_duplicates.txt"),
    shell:
        "python3 scripts/pygenomics_examples/get_duplicates.py "
        "{input} > {output}"


rule add_sequence_counts:
    input:
        summary="data/summaries/{gene}.csv",
        duplicates="data/protein_sequences_with_ref_dedup/{gene}_duplicates.txt",
    output:
        "data/summaries_with_counts/{gene}.csv",
    script:
        "scripts/add_sequence_counts.R"


rule get_simplex_candidates:
    input:
        summary="data/summaries_with_counts/{gene}.csv",
        accessions="data/distances/{gene}_seq_names.txt",
        distances="data/distances/{gene}.txt.gz",
    output:
        full="output/simplices/{gene}_simplex_distances.csv",
        vertices="output/simplices/{gene}_simplices.txt",
    params:
        pangolins="NA BA.5 B.1.617.2 P.1",
    shell:
        "python3 scripts/get_simplex_candidates.py --quiet --full {output.full} "
        " {input.summary} {input.accessions} {input.distances} {params.pangolins} "
        " > {output.vertices}"


REF_ACCESSIONS = {"N": "YP_009724397.2", "S": "YP_009724390.1"}


rule get_distance_box_plots:
    input:
        summary="data/summaries_with_counts/{gene}.csv",
        distances="output/simplices/{gene}_simplex_distances.csv",
    params:
        reference=lambda wildcards: REF_ACCESSIONS[wildcards.gene],
        label=lambda wildcards: str.format(
            "Distances to {:s} for gene {:s}",
            REF_ACCESSIONS[wildcards.gene],
            wildcards.gene,
        ),
    output:
        "output/distance_plots/{gene}.png",
    script:
        "scripts/visualize_distances.R"


rule get_tsne_embedding:
    input:
        summary="data/summaries_with_counts/{gene}.csv",
        distances="data/distances/{gene}.txt.gz",
    params:
        label=lambda wildcards: str.format(
            "{:s} gene, perplexity = {:s}", wildcards.gene, wildcards.perplexity
        ),
        prefix=lambda wildcards: str.format(
            "output/tsne/gene_{:s}_p_{:s}", wildcards.gene, wildcards.perplexity
        ),
        seed=1_000,
        perplexity=lambda wildcards: int(wildcards.perplexity),
        large=lambda wildcards: wildcards.gene in {"N", "S", "ORF3a"},
    threads: workflow.cores // 2
    output:
        "output/tsne/gene_{gene}_p_{perplexity}.csv",
        "output/tsne/gene_{gene}_p_{perplexity}_date.png",
        "output/tsne/gene_{gene}_p_{perplexity}_major.png",
        "output/tsne/gene_{gene}_p_{perplexity}_named.png",
    script:
        "scripts/get_tsne_embedding.R"
